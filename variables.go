package main

import (
	"crypto/rsa"
	"log"

	"github.com/go-openapi/loads"
)

var (
	// Database addresses
	// DBHost     = "127.0.0.1"
	// DBUser     = "dev"
	// DBName     = "api-colab"
	// DBPassword = "devpassword"

	DBHost     = "127.0.0.1"
	DBUser     = "dbuser"
	DBName     = "api-colab-test"
	DBPassword = "dbpass"

	// GenericLogger is the default logger for this application
	GenericLogger *log.Logger

	// PrivateKey describes the matching private key in x509 ASN.1 PKCS#1 DER encoded form
	PrivateKey *rsa.PrivateKey

	// SwaggerDocument is the parsed swagger sepecification struct
	MetaSwaggerDocument *loads.Document

	// AuthenticationOverride is the switch for overriding incoming requests
	// Do not enable in Production mode
	AuthenticationOverride = false

	// TimedLoggingOverride is the switch for overriding timed logging of incoming requests
	// This log can be good benchmark of server performance but may be disabled during Production
	// for resource saving purpose
	// Note: be cautious! This timed log also includes latency to contact OAuth server.
	// Its performance is also subject to network conditions
	TimedLoggingOverride = false
	// TimedLogger handles timed logging (this is meant to benchmark the performance of the server)
	TimedLogger *log.Logger

	// VerboseDBLoggingOverride is the switch for overriding verbose database logging
	VerboseDBLoggingOverride = false
)
