package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/spec"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/validate"
	"github.com/gorilla/mux"

	"gitlab.oit.duke.edu/colab/api-colab/libs/database"
	"gitlab.oit.duke.edu/colab/api-colab/libs/datacontract"
	"gitlab.oit.duke.edu/colab/api-colab/libs/oauth"
)

// DocsHandler handles GET to /docs
func DocsHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	default:
		w.WriteHeader(http.StatusNotFound)
		return
	case http.MethodGet:
		docs, err := database.GetAllDocs()
		if err != nil {
			if e, ok := err.(datacontract.InternalError); ok {
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		docsSwagger := make(map[string]map[string]*spec.Swagger)
		for n, m := range docs {
			for v, d := range m {
				doc, err := loads.Analyzed(d, "2.0")
				if err != nil {
					e := datacontract.InternalLogicsError
					e.ErrorMessage += err.Error()
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
					return
				}
				// Rewrite stuff for SwaggerUI output
				datacontract.PreprocessDocument(doc)
				if _, ok := docsSwagger[n]; !ok {
					docsSwagger[n] = make(map[string]*spec.Swagger)
				}
				docsSwagger[n][v] = doc.Spec()
			}
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(docsSwagger)
		return
	}
}

// APIsHandler handles GET, POST, DELETE to /apis
func APIsHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	default:
		w.WriteHeader(http.StatusNotFound)
		return
	case http.MethodGet:
		apiList, err := database.GetAPIs()
		if err != nil {
			if e, ok := err.(datacontract.InternalError); ok {
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(apiList)
		return
	case http.MethodPost, http.MethodDelete:
		// Prepare information
		// Prepare OAuth Token
		oAuthToken, err := extractOAuthToken(r)
		if err != nil {
			if e, ok := err.(datacontract.InternalError); ok {
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		// Validate required scopes against allowed scopes
		var eppn string
		var scopeErr error
		if r.Method == http.MethodPost {
			eppn, scopeErr = oauth.ValidateScope(oAuthToken, "/apis", http.MethodPost, MetaSwaggerDocument)
			if scopeErr != nil {
				if e, ok := scopeErr.(datacontract.InternalError); ok {
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				} else {
					e := datacontract.InternalLogicsError
					e.ErrorMessage += err.Error()
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				}
				return
			}
		} else {
			eppn, scopeErr = oauth.ValidateScope(oAuthToken, "/apis", http.MethodDelete, MetaSwaggerDocument)
			if scopeErr != nil {
				if e, ok := scopeErr.(datacontract.InternalError); ok {
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				} else {
					e := datacontract.InternalLogicsError
					e.ErrorMessage += err.Error()
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				}
				return
			}
		}
		// Read body
		reqBody, err := ioutil.ReadAll(r.Body)
		r.Body.Close()
		if err != nil {
			e := datacontract.RequestNoBodyError
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		if len(reqBody) == 0 {
			e := datacontract.RequestNoBodyError
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		// Parse request body as Swagger specification
		incomingDoc, err := loads.Analyzed(reqBody, "2.0")
		if err != nil {
			e := datacontract.RequestBodyToSwaggerError
			e.ErrorMessage += err.Error()
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		// Validate incoming body against Swagger specification only when create/update
		if r.Method == http.MethodPost {
			if err := validate.Spec(incomingDoc, strfmt.Default); err != nil {
				e := datacontract.RequestBodySwaggerValidationError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
				return
			}
		}
		// Confirm existence of x-name field in info because this is used as the well-known name of the sub API
		xName, ok := incomingDoc.Spec().Info.Extensions.GetString("x-name")
		if !ok {
			e := datacontract.RequestBodySwaggerNoXNameError
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		// Validate x-name. Allows only alphanumerics, underscores, and dashes
		pattern := "([[:alnum:]]|_|-)+"
		if ok, err := regexp.MatchString(pattern, xName); !ok || err != nil {
			e := datacontract.RequestBodySwaggerXNameFormatError
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		// Confirm existence of version field
		apiVersion := incomingDoc.Spec().Info.Version
		if apiVersion == "" {
			e := datacontract.RequestBodySwaggerNoVersionError
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		// Validate version. Allows only alphanumerics, underscores, and dashes
		if ok, err := regexp.MatchString(pattern, apiVersion); !ok || err != nil {
			e := datacontract.RequestBodySwaggerVersionFormatError
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		// Confirm existence of host
		if incomingDoc.Host() == "" {
			e := datacontract.RequestBodySwaggerNoHostError
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		// ensure secdef and scheme exist before add or update API
		if r.Method == http.MethodPost {
			secDefMap := incomingDoc.Spec().SecurityDefinitions
			if secDefMap == nil {
				e := datacontract.RequestBodySwaggerNoSecDefError
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
				return
			}
			schemes := incomingDoc.Spec().Schemes
			if schemes == nil {
				e := datacontract.RequestBodySwaggerNoSchemesError
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
				return
			}
			// ensure strict HTTPS scheme
			foundHTTPS := false
			for _, v := range schemes {
				if v == "https" {
					foundHTTPS = true
					break
				}
			}
			if len(schemes) > 1 || !foundHTTPS {
				e := datacontract.RequestBodySwaggerSchemesMismatchError
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
				return
			}
		}
		// Have the database store the API if create/update
		if r.Method == http.MethodPost {
			if err := database.AddOrUpdateAPI(incomingDoc, eppn); err != nil {
				if e, ok := err.(datacontract.InternalError); ok {
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				} else {
					e := datacontract.InternalLogicsError
					e.ErrorMessage += err.Error()
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				}
				return
			}
			// Report 201 if no error found
			w.WriteHeader(http.StatusCreated)
			return
		}
		// r.Method == "DELETE"
		// Have the database delete the API if removal
		if err := database.RemoveAPI(incomingDoc, eppn); err != nil {
			if e, ok := err.(datacontract.InternalError); ok {
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		// Report 204 if no error found
		w.WriteHeader(http.StatusNoContent)
		return
	}
}

// SpecificAPIHandler handles GET /apis/{api}/{version}
func SpecificAPIHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	default:
		w.WriteHeader(http.StatusNotFound)
		return
	case http.MethodGet:
		vars := mux.Vars(r)
		xName := vars["api"]
		version := vars["version"]
		swaggerDocRaw, err := database.GetParticularAPI(xName, version)
		if err != nil {
			if e, ok := err.(datacontract.InternalError); ok {
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		swaggerDoc, err := loads.Analyzed(swaggerDocRaw, "2.0")
		if err != nil {
			e := datacontract.SystemError
			e.ErrorMessage += e.Error()
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		datacontract.PreprocessDocument(swaggerDoc)
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(swaggerDoc.Spec())
		return
	}
}

// AppsHandler handles GET, POST, DELETE to /apps
func AppsHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	default:
		w.WriteHeader(http.StatusNotFound)
		return
	case http.MethodGet:
		// Prepare information
		// Prepare OAuth Token
		oAuthToken, err := extractOAuthToken(r)
		if err != nil {
			if e, ok := err.(datacontract.InternalError); ok {
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		// Validate required scopes against allowed scopes
		eppn, err := oauth.ValidateScope(oAuthToken, "/apps", http.MethodGet, MetaSwaggerDocument)
		if err != nil {
			if e, ok := err.(datacontract.InternalError); ok {
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		appList, err := database.GetAppsAsUser(eppn)
		if err != nil {
			if e, ok := err.(datacontract.InternalError); ok {
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(appList)
		return
	case http.MethodPost, http.MethodDelete:
		// Prepare information
		// Prepare OAuth Token
		oAuthToken, err := extractOAuthToken(r)
		if err != nil {
			if e, ok := err.(datacontract.InternalError); ok {
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		// Validate required scopes against allowed scopes
		var eppn string
		var scopeErr error
		if r.Method == http.MethodPost {
			eppn, scopeErr = oauth.ValidateScope(oAuthToken, "/apps", http.MethodPost, MetaSwaggerDocument)
			if scopeErr != nil {
				if e, ok := scopeErr.(datacontract.InternalError); ok {
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				} else {
					e := datacontract.InternalLogicsError
					e.ErrorMessage += err.Error()
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				}
				return
			}
		} else { // r.Method == DELETE
			eppn, scopeErr = oauth.ValidateScope(oAuthToken, "/apps", http.MethodDelete, MetaSwaggerDocument)
			if scopeErr != nil {
				if e, ok := scopeErr.(datacontract.InternalError); ok {
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				} else {
					e := datacontract.InternalLogicsError
					e.ErrorMessage += err.Error()
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				}
				return
			}
		}
		// Decode request body
		var incomingApp datacontract.ApplicationForHTTP
		if err := json.NewDecoder(r.Body).Decode(&incomingApp); err != nil {
			e := datacontract.RequestAppBodyParseError
			e.ErrorMessage += err.Error()
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		// Validate incoming Application
		// validate clientID, allow only alphanumerics, underscores and dashes
		pattern := "([[:alnum:]]|_|-)+"
		if ok, err := regexp.MatchString(pattern, incomingApp.ClientID); !ok || err != nil {
			e := datacontract.RequestAppBodyClientIDFormatError
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		// validate redirect URIs
		for _, v := range incomingApp.RedirectURIs {
			_, err := url.Parse(v)
			if err != nil {
				e := datacontract.RequestAppBodyRedirectURIFormatError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
				return
			}
		}
		// validate Privacy URL
		if _, err := url.Parse(incomingApp.PrivacyURL); err != nil {
			e := datacontract.RequestAppBodyPrivacyURIFormatError
			e.ErrorMessage += err.Error()
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
			return
		}
		// The rest of the fields in incoming application is pushed to
		// OAuth validation.
		// Error from OAuth server will be gathered and returned to the requesting client
		if r.Method == http.MethodPost {
			var newApp *datacontract.ApplicationForHTTP
			if newApp, err = database.AddOrUpdateApp(incomingApp, eppn, oAuthToken); err != nil {
				if e, ok := err.(datacontract.InternalError); ok {
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				} else {
					e := datacontract.InternalLogicsError
					e.ErrorMessage += err.Error()
					w.WriteHeader(e.HTTPStatusCode)
					json.NewEncoder(w).Encode(e)
				}
				return
			}
			w.WriteHeader(http.StatusCreated)
			json.NewEncoder(w).Encode(*newApp)
			return
		}
		// r.Method == "DELETE"
		if err := database.RemoveApp(incomingApp, eppn, oAuthToken); err != nil {
			if e, ok := err.(datacontract.InternalError); ok {
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		w.WriteHeader(http.StatusNoContent)
		return
	}
}

// StatusHandler handles GET to /status
func StatusHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	default:
		w.WriteHeader(http.StatusNotFound)
	case http.MethodGet:
	}
}
