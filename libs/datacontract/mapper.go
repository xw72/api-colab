package datacontract

import (
	"fmt"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/spec"
)

// PreprocessDocument changes some fields in the Swagger document stored in the database.
// It makes the returned document suitable for use by Swagger UI
func PreprocessDocument(doc *loads.Document) {
	xName, _ := doc.Spec().Info.Extensions.GetString("x-name")
	apiVersion := doc.Spec().Info.Version
	// Rewrite fields in doc to comply with api-colab specifics
	// Rewrite basePath
	doc.Spec().BasePath = fmt.Sprintf("/%s/%s", xName, apiVersion)
	// Rewrite host
	doc.Spec().Host = APISyndicationServiceURI
	// Rewrite headers for each operation in each relative path
	// This is to hide x-netid and ensure security field has api_key when security definition exists
	// This is meant only for Swagger UI
	pathMap := doc.Spec().Paths.Paths
	// NOTE: Before thinking about optimizing the following code for repetition.
	// Read this and consider the trade off: http://stackoverflow.com/questions/18930910/golang-access-struct-property-by-name
	// The trade-off is between compiler optimization for static struct field names (verbose repeated code) and unoptimized dynamic field name detection (no repeated code)
	for _, p := range pathMap {
		if p.Get != nil {
			// remove x-netid from parameter
			param := p.Get.Parameters
			// for time efficiency, only copy over valid value instead of deleting invalid value while iterating
			// pre allocation to avoid extensive memory operation
			paramValid := make([]spec.Parameter, 0, len(param))
			for _, v := range param {
				if v.Name != InternalIdHeaderName && v.Name != InternalIdHeaderNameAlternative {
					paramValid = append(paramValid, v)
				}
			}
			p.Get.Parameters = paramValid
			// Confirm api_key when security exists
			sec := p.Get.Security
			if len(sec) > 0 {
				found := false
				for _, se := range sec {
					if _, ok := se["api_key"]; ok {
						se["api_key"] = []string{}
					}
				}
				if !found {
					sec = append(sec, map[string][]string{
						"api_key": []string{},
					})
				}
				p.Get.Security = sec
			}
		}
		if p.Put != nil {
			// remove x-netid from parameter
			param := p.Put.Parameters
			// for time efficiency, only copy over valid value instead of deleting invalid value while iterating
			// pre allocation to avoid extensive memory operation
			paramValid := make([]spec.Parameter, 0, len(param))
			for _, v := range param {
				if v.Name != InternalIdHeaderName && v.Name != InternalIdHeaderNameAlternative {
					paramValid = append(paramValid, v)
				}
			}
			p.Put.Parameters = paramValid
			// Confirm api_key when security exists
			sec := p.Put.Security
			if len(sec) > 0 {
				found := false
				for _, se := range sec {
					if _, ok := se["api_key"]; ok {
						se["api_key"] = []string{}
					}
				}
				if !found {
					sec = append(sec, map[string][]string{
						"api_key": []string{},
					})
				}
				p.Put.Security = sec
			}
		}
		if p.Post != nil {
			// remove x-netid from parameter
			param := p.Post.Parameters
			// for time efficiency, only copy over valid value instead of deleting invalid value while iterating
			// pre allocation to avoid extensive memory operation
			paramValid := make([]spec.Parameter, 0, len(param))
			for _, v := range param {
				if v.Name != InternalIdHeaderName && v.Name != InternalIdHeaderNameAlternative {
					paramValid = append(paramValid, v)
				}
			}
			p.Post.Parameters = paramValid
			// Confirm api_key when security exists
			sec := p.Post.Security
			if len(sec) > 0 {
				found := false
				for _, se := range sec {
					if _, ok := se["api_key"]; ok {
						se["api_key"] = []string{}
					}
				}
				if !found {
					sec = append(sec, map[string][]string{
						"api_key": []string{},
					})
				}
				p.Post.Security = sec
			}
		}
		if p.Delete != nil {
			// remove x-netid from parameter
			param := p.Delete.Parameters
			// for time efficiency, only copy over valid value instead of deleting invalid value while iterating
			// pre allocation to avoid extensive memory operation
			paramValid := make([]spec.Parameter, 0, len(param))
			for _, v := range param {
				if v.Name != InternalIdHeaderName && v.Name != InternalIdHeaderNameAlternative {
					paramValid = append(paramValid, v)
				}
			}
			p.Delete.Parameters = paramValid
			// Confirm api_key when security exists
			sec := p.Delete.Security
			if len(sec) > 0 {
				found := false
				for _, se := range sec {
					if _, ok := se["api_key"]; ok {
						se["api_key"] = []string{}
					}
				}
				if !found {
					sec = append(sec, map[string][]string{
						"api_key": []string{},
					})
				}
				p.Delete.Security = sec
			}
		}
		if p.Options != nil {
			// remove x-netid from parameter
			param := p.Options.Parameters
			// for time efficiency, only copy over valid value instead of deleting invalid value while iterating
			// pre allocation to avoid extensive memory operation
			paramValid := make([]spec.Parameter, 0, len(param))
			for _, v := range param {
				if v.Name != InternalIdHeaderName && v.Name != InternalIdHeaderNameAlternative {
					paramValid = append(paramValid, v)
				}
			}
			p.Options.Parameters = paramValid
			// Confirm api_key when security exists
			sec := p.Options.Security
			if len(sec) > 0 {
				found := false
				for _, se := range sec {
					if _, ok := se["api_key"]; ok {
						se["api_key"] = []string{}
					}
				}
				if !found {
					sec = append(sec, map[string][]string{
						"api_key": []string{},
					})
				}
				p.Options.Security = sec
			}
		}
		if p.Head != nil {
			// remove x-netid from parameter
			param := p.Head.Parameters
			// for time efficiency, only copy over valid value instead of deleting invalid value while iterating
			// pre allocation to avoid extensive memory operation
			paramValid := make([]spec.Parameter, 0, len(param))
			for _, v := range param {
				if v.Name != InternalIdHeaderName && v.Name != InternalIdHeaderNameAlternative {
					paramValid = append(paramValid, v)
				}
			}
			p.Head.Parameters = paramValid
			// Confirm api_key when security exists
			sec := p.Head.Security
			if len(sec) > 0 {
				found := false
				for _, se := range sec {
					if _, ok := se["api_key"]; ok {
						se["api_key"] = []string{}
					}
				}
				if !found {
					sec = append(sec, map[string][]string{
						"api_key": []string{},
					})
				}
				p.Head.Security = sec
			}
		}
		if p.Patch != nil {
			// remove x-netid from parameter
			param := p.Patch.Parameters
			// for time efficiency, only copy over valid value instead of deleting invalid value while iterating
			// pre allocation to avoid extensive memory operation
			paramValid := make([]spec.Parameter, 0, len(param))
			for _, v := range param {
				if v.Name != InternalIdHeaderName && v.Name != InternalIdHeaderNameAlternative {
					paramValid = append(paramValid, v)
				}
			}
			p.Patch.Parameters = paramValid
			// Confirm api_key when security exists
			sec := p.Patch.Security
			if len(sec) > 0 {
				found := false
				for _, se := range sec {
					if _, ok := se["api_key"]; ok {
						se["api_key"] = []string{}
					}
				}
				if !found {
					sec = append(sec, map[string][]string{
						"api_key": []string{},
					})
				}
				p.Patch.Security = sec
			}
		}
	}
}
