package database

import (
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"gitlab.oit.duke.edu/colab/api-colab/libs/datacontract"
)

var (
	db       *gorm.DB
	dbLogger *log.Logger
)

// InitAndConnect synchonously initialises database
// Leave dbLogFile to nil if logging to stdout
func InitAndConnect(dbHost string, dbUser string, dbPassword string, dbName string, verboseLog bool, dbLogFile *os.File) error {
	if dbLogFile == nil {
		dbLogger = log.New(os.Stdout, "[API.Colab Database] ", log.LstdFlags)
	} else {
		dbLogger = log.New(dbLogFile, "[API.Colab Database] ", log.LstdFlags)
	}

	dbConn, err := gorm.Open("postgres", fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, dbUser, dbName, dbPassword))
	if err != nil {
		return errors.New("Cannot open connection to database " + err.Error())
	}
	db = dbConn

	// check for verbose logging
	if verboseLog {
		dbLogger.Println("verbose DB logging enabled")
		db.LogMode(true)
	}

	db.AutoMigrate(
		&datacontract.Admin{},
		&datacontract.API{},
		&datacontract.APIOwner{},
		&datacontract.ApplicationForDB{},
		&datacontract.AppOwner{},
	)

	// TODO: Check if /meta is in database
	// This is only necessary for Swagger UI. It may no longer be true to include /meta in database.

	dbLogger.Println("Database auto migration complete!")
	return nil
}

// Seed puts preliminary records in the database
// This can be used for testing purposes
// and actual data seeding
func Seed(model interface{}) error {
	tx := db.Begin()
	if err := tx.FirstOrCreate(model).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		return err
	}
	return nil
}

// Close closes the database connection
func Close() error {
	return db.Close()
}

func CleanAll() {
	if datacontract.GoEnv == "test" {
		db.Exec("DELETE FROM admins;")
		db.Exec("DELETE FROM api_owners;")
		db.Exec("DELETE FROM api_owners_apis;")
		db.Exec("DELETE FROM apis;")
		db.Exec("DELETE FROM app_owners;")
		db.Exec("DELETE FROM app_owners_application_for_dbs;")
		db.Exec("DELETE FROM application_for_dbs;")
	}
}
