package datacontract

const (
	// APISyndicationServiceURI is the URI for syndication services
	APISyndicationServiceURI = "api.colab.duke.edu"

	// InternalIdHeaderName is the name for internal
	// id information of a person
	InternalIdHeaderName = "x-netid"

	// InternalIdHeaderNameAlternative is the name
	// for alternative internal id information of a person
	InternalIdHeaderNameAlternative = "x-eppn"

	// XAPIHeader is meant for passing client id that tracks the origin of the request
	APIKeyHeaderName = "x-api-key"
)
