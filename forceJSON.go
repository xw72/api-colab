package main

import (
	"net/http"
)

type forceJSONHandler struct {
	handler http.Handler
}

// ForceJSONHandler inserts content-type header to all responses
func ForceJSONHandler(h http.Handler) http.Handler {
	return forceJSONHandler{handler: h}
}

func (forceJSON forceJSONHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("content-type", "application/json; charset=UTF-8")
	forceJSON.handler.ServeHTTP(w, req)
}
