package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.oit.duke.edu/colab/api-colab/libs/database"
	"gitlab.oit.duke.edu/colab/api-colab/libs/datacontract"
	"gitlab.oit.duke.edu/colab/api-colab/libs/oauth"
)

var (
	testAPIContent []byte
	testAppContent []byte

	oAuthValidationServerEmulator = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.Write([]byte(`{
			"eppn":  "test@duke.edu",
			"scope": "meta:api:write meta:apps:write meta:apps:read oauth_registrations"
    }`))
	}))
	oAuthAppRegisterServerEmulator = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.Write(testAppContent)
	}))
	oAuthAppRevokeServerEmulator = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNoContent)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	}))

	testServer = httptest.NewServer(ConfigureRoutingAndMiddleware())
)

func TestMain(m *testing.M) {
	datacontract.GoEnv = "test"
	DBHost = "127.0.0.1"
	DBName = "api-colab-test"
	DBUser = "test"
	DBPassword = "testpass"
	database.InitAndConnect(DBHost, DBUser, DBPassword, DBName, false, os.Stdout)
	database.CleanAll()
	testAppOwner := datacontract.AppOwner{EPPN: "test@duke.edu"}
	database.Seed(&testAppOwner)
	testApp := datacontract.ApplicationForDB{
		ClientID:         "test",
		ClientSecret:     "somesecret",
		RedirectURIs:     []byte(`["http://localhost:3000/"]`),
		DisplayName:      "test-displayName",
		Description:      "test-desc",
		OwnerDescription: "test-owner",
		PrivacyURL:       "http://localhost:3000/privacy",
		Permissions:      []byte(`[{"service": "basic", "access": "full"}]`),
		AppOwners: []datacontract.AppOwner{
			testAppOwner,
		},
	}
	database.Seed(&testApp)
	file, err := os.Open("./testFiles/testAPI.json")
	if err != nil {
		panic(err)
	}
	testAPIContent, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}
	file, err = os.Open("./testFiles/testApp.json")
	if err != nil {
		panic(err)
	}
	testAppContent, err = ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}
	testAPIOwner := datacontract.APIOwner{EPPN: "test@duke.edu"}
	database.Seed(&testAPIOwner)
	testAPI := datacontract.API{
		XName:   "test",
		Host:    "petstore.swagger.io",
		Version: "v1",
		Owners: []datacontract.APIOwner{
			testAPIOwner,
		},
		SwaggerDocRaw: testAPIContent,
	}
	database.Seed(&testAPI)
	defer testServer.Close()
	defer database.CleanAll()
	defer database.Close()
	ParseSpec()
	ParsePrivateKeyFromFile("./ssl/api-colab-duke-edu.key")
	os.Exit(m.Run())
}

func TestRequestXAPIHeader(t *testing.T) {
	t.Run("RequestWithXAPIHeader", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodGet, testServer.URL+"/meta/v1/docs", nil)
		if err != nil {
			t.Error(err)
		}
		req.Header.Add(datacontract.APIKeyHeaderName, "test")
		resp, err := http.DefaultClient.Do(req)
		if err != nil || resp.StatusCode != http.StatusOK {
			t.Fail()
		}
	})
	t.Run("RequestWithoutAPIHeader", func(t *testing.T) {
		resp, err := http.Get(testServer.URL + "/meta/v1/docs")
		if err != nil {
			t.Error(err)
		}
		if resp.StatusCode != http.StatusBadRequest {
			t.Fail()
		}
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Error(err)
		}
		resp.Body.Close()
		n := bytes.IndexByte(respBody, '\n')
		if string(respBody[:n]) != "{\"errorCode\":400124,\"errorMessage\":\"No X-API-Key is present in request header\"}" {
			t.Fail()
		}
	})
}

func TestRequestOAuthAuthorizationHeader(t *testing.T) {
	oauth.OAuthValidationURI = oAuthValidationServerEmulator.URL
	oauth.OAuthRegisterAppURI = oAuthAppRegisterServerEmulator.URL
	oauth.OAuthRevokeAppURI = oAuthAppRevokeServerEmulator.URL
	t.Run("NoAuthorizationHeaderWhenNotRequired", func(t *testing.T) {
		t.Run("GET /meta/v1/docs", func(t *testing.T) {
			req, err := http.NewRequest(http.MethodGet, testServer.URL+"/meta/v1/docs", nil)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusOK {
				t.Fail()
			}
		})
		t.Run("GET /meta/v1/apis", func(t *testing.T) {
			req, err := http.NewRequest(http.MethodGet, testServer.URL+"/meta/v1/apis", nil)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusOK {
				t.Fail()
			}
		})
		t.Run("GET /meta/v1/apis/{api}/{version}", func(t *testing.T) {
			req, err := http.NewRequest(http.MethodGet, testServer.URL+"/meta/v1/apis/test/v1", nil)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusOK {
				t.Fail()
			}
		})
	})
	t.Run("NoAuthorizationHeaderWhenRequired", func(t *testing.T) {
		t.Run("POST /meta/v1/apis", func(t *testing.T) {
			file, err := os.Open("./testFiles/testAPI.json")
			if err != nil {
				t.Error(err)
			}
			req, err := http.NewRequest(http.MethodPost, testServer.URL+"/meta/v1/apis", file)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusBadRequest {
				t.Fail()
			}
			respBody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Error(err)
			}
			resp.Body.Close()
			n := bytes.IndexByte(respBody, '\n')
			if string(respBody[:n]) != "{\"errorCode\":400105,\"errorMessage\":\"no authorization header provided\"}" {
				t.Fail()
			}
		})
		t.Run("DELETE /meta/v1/apis", func(t *testing.T) {
			file, err := os.Open("./testFiles/testAPI.json")
			if err != nil {
				t.Error(err)
			}
			req, err := http.NewRequest(http.MethodDelete, testServer.URL+"/meta/v1/apis", file)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusBadRequest {
				t.Fail()
			}
			respBody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Error(err)
			}
			resp.Body.Close()
			n := bytes.IndexByte(respBody, '\n')
			if string(respBody[:n]) != "{\"errorCode\":400105,\"errorMessage\":\"no authorization header provided\"}" {
				t.Fail()
			}
		})
		t.Run("GET /meta/v1/apps", func(t *testing.T) {
			req, err := http.NewRequest(http.MethodGet, testServer.URL+"/meta/v1/apps", nil)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusBadRequest {
				t.Fail()
			}
			respBody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Error(err)
			}
			resp.Body.Close()
			n := bytes.IndexByte(respBody, '\n')
			if string(respBody[:n]) != "{\"errorCode\":400105,\"errorMessage\":\"no authorization header provided\"}" {
				t.Fail()
			}
		})
		t.Run("POST /meta/v1/apps", func(t *testing.T) {
			file, err := os.Open("./testFiles/testApp.json")
			if err != nil {
				t.Error(err)
			}
			req, err := http.NewRequest(http.MethodPost, testServer.URL+"/meta/v1/apps", file)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusBadRequest {
				t.Fail()
			}
			respBody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Error(err)
			}
			resp.Body.Close()
			n := bytes.IndexByte(respBody, '\n')
			if string(respBody[:n]) != "{\"errorCode\":400105,\"errorMessage\":\"no authorization header provided\"}" {
				t.Fail()
			}
		})
		t.Run("DELETE /meta/v1/apps", func(t *testing.T) {
			file, err := os.Open("./testFiles/testApp.json")
			if err != nil {
				t.Error(err)
			}
			req, err := http.NewRequest(http.MethodDelete, testServer.URL+"/meta/v1/apps", file)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusBadRequest {
				t.Fail()
			}
			respBody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Error(err)
			}
			resp.Body.Close()
			n := bytes.IndexByte(respBody, '\n')
			if string(respBody[:n]) != "{\"errorCode\":400105,\"errorMessage\":\"no authorization header provided\"}" {
				t.Fail()
			}
		})
	})
	t.Run("GoodAuthorizationHeaderWhenRequired", func(t *testing.T) {
		t.Run("POST /meta/v1/apis", func(t *testing.T) {
			file, err := os.Open("./testFiles/testAPI.json")
			if err != nil {
				t.Error(err)
			}
			req, err := http.NewRequest(http.MethodPost, testServer.URL+"/meta/v1/apis", file)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			req.Header.Add("Authorization", "Bearer sometoken")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusCreated {
				t.Fail()
			}
		})
		t.Run("DELETE /meta/v1/apis", func(t *testing.T) {
			file, err := os.Open("./testFiles/testAPI.json")
			if err != nil {
				t.Error(err)
			}
			req, err := http.NewRequest(http.MethodDelete, testServer.URL+"/meta/v1/apis", file)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			req.Header.Add("Authorization", "Bearer sometoken")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusNoContent {
				t.Fail()
			}
		})
		t.Run("GET /meta/v1/apps", func(t *testing.T) {
			req, err := http.NewRequest(http.MethodGet, testServer.URL+"/meta/v1/apps", nil)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			req.Header.Add("Authorization", "Bearer sometoken")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusOK {
				t.Fail()
			}
		})
		t.Run("POST /meta/v1/apps", func(t *testing.T) {
			file, err := os.Open("./testFiles/testApp.json")
			if err != nil {
				t.Error(err)
			}
			req, err := http.NewRequest(http.MethodPost, testServer.URL+"/meta/v1/apps", file)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			req.Header.Add("Authorization", "Bearer sometoken")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusCreated {
				t.Fail()
			}
		})
		t.Run("DELETE /meta/v1/apps", func(t *testing.T) {
			file, err := os.Open("./testFiles/testApp.json")
			if err != nil {
				t.Error(err)
			}
			req, err := http.NewRequest(http.MethodDelete, testServer.URL+"/meta/v1/apps", file)
			if err != nil {
				t.Error(err)
			}
			req.Header.Add(datacontract.APIKeyHeaderName, "test")
			req.Header.Add("Authorization", "Bearer sometoken")
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Error(err)
			}
			if resp.StatusCode != http.StatusNoContent {
				t.Fail()
			}
		})
	})
}

func TestReqeustCORS(t *testing.T) {
	t.Run("GoodCORSRequests", func(t *testing.T) {
		t.Run("GoodHTTPMethods", func(t *testing.T) {
			t.Run("GET", func(t *testing.T) {
				req, err := http.NewRequest(http.MethodGet, testServer.URL+"/meta/v1/status", nil)
				if err != nil {
					t.Error(err)
				}
				// req.Header.Add("access-control-request-headers", "Authorization,X-API-Key")
				// req.Header.Add("access-control-request-method", http.MethodGet)
				req.Header.Add("origin", "http://anyorigin")
				req.Header.Add("x-api-key", "test")
				resp, err := http.DefaultClient.Do(req)
				if err != nil {
					t.Error(err)
				}
				fmt.Println(resp)
				if resp.StatusCode != http.StatusOK {
					t.Fail()
				}
				if allowed := resp.Header.Get("access-control-allow-origin"); allowed != "http://anyorigin" {
					t.Fail()
				}
			})
			t.Run("POST", func(t *testing.T) {

			})
			t.Run("PUT", func(t *testing.T) {

			})
			t.Run("OPTIONS", func(t *testing.T) {

			})
			t.Run("DELETE", func(t *testing.T) {

			})
		})
	})
}

// func TestOAuthGoodRequest(t *testing.T) {
//
// }
//
// func TestAuthenticationHandler(t *testing.T) {
// 	t.Skip("trivial")
// }
//
// func TestScopeValidationHandler(t *testing.T) {
// 	t.Parallel()
// 	ParseSpec()
// 	// testing good uri
// 	OAuthValidationURI = goodOauthServer.URL
// 	eppn, statusCode, err := ScopeValidationHandler("someoauthtoken", "/apis", http.MethodPost)
// 	if err != nil {
// 		t.Error("good uri has err ", err)
// 	}
// 	if eppn == "" || eppn != "abcd@duke.edu" {
// 		t.Error("eppn mismatch ", eppn)
// 	}
// 	if statusCode != http.StatusOK {
// 		t.Error("status code mismatch ", statusCode)
// 	}
// 	// testing expired uri
// 	OAuthValidationURI = errorOauthServer.URL
// 	eppn, statusCode, err = ScopeValidationHandler("someoauthtoken", "/apis", http.MethodPost)
// 	if err == nil || err.Error() != "expired_token" {
// 		t.Error("expired has err ", err)
// 	}
// 	if eppn != "" {
// 		t.Fail()
// 	}
// 	if statusCode != http.StatusBadRequest {
// 		t.Fail()
// 	}
// 	// testing insufficient scope
// 	OAuthValidationURI = insufficientScopeServer.URL
// 	eppn, statusCode, err = ScopeValidationHandler("someoauthtoken", "/apis", http.MethodPost)
// 	if err == nil || !strings.Contains(err.Error(), "token is not authorized for required scope") {
// 		t.Error("insufficient scope has wrong err ", err)
// 	}
// 	if eppn != "" {
// 		t.Fail()
// 	}
// 	if statusCode != http.StatusBadRequest {
// 		t.Fail()
// 	}
// }
//
// func TestGetAllowedScopeAndEppn(t *testing.T) {
// 	t.Parallel()
// 	// testing good URI
// 	OAuthValidationURI = goodOauthServer.URL
// 	eppn, scopeMap, err := getAllowedScopeAndEppn("someoauthtoken")
// 	if err != nil {
// 		t.Error("good URI has error ", err)
// 	}
// 	if eppn != "abcd@duke.edu" {
// 		t.Errorf("eppn not equal %s should be %s", eppn, "abcd@duke.edu")
// 	}
// 	if scopeMap == nil {
// 		t.Error("no scope map")
// 	}
// 	if _, ok := scopeMap["meta:api:write"]; !ok {
// 		t.Error("no meta:api:write scope")
// 	}
// 	if _, ok := scopeMap["meta:apps:write"]; !ok {
// 		t.Error("no meta:apps:write")
// 	}
// 	if _, ok := scopeMap["meta:apps:read"]; !ok {
// 		t.Error("no meta:apps:read")
// 	}
// 	if _, ok := scopeMap["oauth_registrations"]; !ok {
// 		t.Error("no oauth_registrations")
// 	}
// 	if _, ok := scopeMap["identity:netid:read"]; !ok {
// 		t.Error("no identity:netid:read")
// 	}
// 	// testing bad URI
// 	OAuthValidationURI = errorOauthServer.URL
// 	eppn, scopeMap, err = getAllowedScopeAndEppn("someoauthtoken")
// 	if err == nil {
// 		t.Error("expected error but none is present")
// 	}
// 	if eppn != "" {
// 		t.Error("eppn should be empty string, but is ", eppn)
// 	}
// 	if scopeMap != nil {
// 		t.Error("eppn should be nil, but is ", scopeMap)
// 	}
// 	if err.Error() != "expired_token" {
// 		t.Error("wrong error message, expected expired_token, get", err.Error())
// 	}
// }
//
// func TestExtractOAuthToken(t *testing.T) {
// 	t.Parallel()
// 	req := httptest.NewRequest(http.MethodGet, "/", nil)
// 	req.Header.Set("Authorization", "Bearer abcde12345678")
// 	token, err := extractOAuthToken(req)
// 	if err != nil {
// 		t.Error("Failed to exatract token", err)
// 	}
// 	if token != "abcde12345678" {
// 		t.Fail()
// 	}
// }
