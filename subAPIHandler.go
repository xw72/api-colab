package main

import (
	"crypto"
	rd "crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-openapi/loads"
	"github.com/gorilla/mux"

	"gitlab.oit.duke.edu/colab/api-colab/libs/database"
	"gitlab.oit.duke.edu/colab/api-colab/libs/datacontract"
	"gitlab.oit.duke.edu/colab/api-colab/libs/oauth"
)

// SubAPIHandler handles any API call against sub API endpoints
func SubAPIHandler(w http.ResponseWriter, r *http.Request) {
	// Extract path variables and confirm existence of subAPI
	vars := mux.Vars(r)
	apiName := vars["apiName"]
	version := vars["version"]
	requestPath := vars["path"]
	GenericLogger.Println(apiName, version, requestPath)
	docRaw, err := database.GetParticularAPI(apiName, version)
	if err != nil {
		if e, ok := err.(datacontract.InternalError); ok {
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
		} else {
			e := datacontract.InternalLogicsError
			e.ErrorMessage += err.Error()
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
		}
		return
	}
	subDoc, err := loads.Analyzed(docRaw, "2.0")
	if err != nil {
		e := datacontract.InternalLogicsError
		e.ErrorMessage += err.Error()
		w.WriteHeader(e.HTTPStatusCode)
		json.NewEncoder(w).Encode(e)
		return
	}
	// create new http request
	var forwardedReq *http.Request
	if len(r.URL.Query()) != 0 {
		forwardedReq, err = http.NewRequest(r.Method, fmt.Sprintf("https://%s%s/%s?%s", subDoc.Host(), subDoc.BasePath(), requestPath, r.URL.Query().Encode()), r.Body)
	} else {
		forwardedReq, err = http.NewRequest(r.Method, fmt.Sprintf("https://%s%s/%s", subDoc.Host(), subDoc.BasePath(), requestPath), r.Body)
	}
	if err != nil {
		e := datacontract.InternalLogicsError
		e.ErrorMessage += err.Error()
		w.WriteHeader(e.HTTPStatusCode)
		json.NewEncoder(w).Encode(e)
		return
	}
	// copy all headers from incoming request
	forwardedReq.Header = r.Header
	// Append OAuth token and an "x-netid" header if OAuth Token can be extracted
	// NOTE: this "x-netid" header is essentially what is requested by subAPI.
	// The primary functionality of api-colab when forwarding requests is to exchange OAuth token for an EPPN (netID)
	if oAuthToken, err := extractOAuthToken(r); err == nil {
		eppn, scopeErr := oauth.ValidateExpiration(oAuthToken)
		if scopeErr != nil {
			if e, ok := scopeErr.(datacontract.InternalError); ok {
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			} else {
				e := datacontract.InternalLogicsError
				e.ErrorMessage += err.Error()
				w.WriteHeader(e.HTTPStatusCode)
				json.NewEncoder(w).Encode(e)
			}
			return
		}
		forwardedReq.Header.Set("x-netid", strings.Split(eppn, "@")[0])
	}
	// Generate signature header
	randomNum := rand.Float32()
	asStr := strconv.FormatFloat(float64(randomNum), 'f', -1, 32)
	forwardedReq.Header.Set("x-random", asStr)
	hashed := sha512.Sum512([]byte(asStr))
	signature, err := rsa.SignPKCS1v15(rd.Reader, PrivateKey, crypto.SHA512, hashed[:])
	if err != nil {
		e := datacontract.InternalLogicsError
		e.ErrorMessage += err.Error()
		w.WriteHeader(e.HTTPStatusCode)
		json.NewEncoder(w).Encode(e)
		return
	}
	forwardedReq.Header.Set("x-signature", base64.StdEncoding.EncodeToString(signature))
	// send the request
	resp, err := http.DefaultClient.Do(forwardedReq)
	if err != nil {
		e := datacontract.InternalLogicsError
		e.ErrorMessage += err.Error()
		w.WriteHeader(e.HTTPStatusCode)
		json.NewEncoder(w).Encode(e)
		return
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		e := datacontract.SubServiceUnavailableError
		w.WriteHeader(e.HTTPStatusCode)
		json.NewEncoder(w).Encode(e)
		return
	}
	w.WriteHeader(resp.StatusCode)
	w.Write(respBody)
}
