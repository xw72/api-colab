package database

// NOTE: dbCursor.Error() cantains record not found error if it happens
// therefore, always check cursor.RecordNotFound() first
// in this way, a record not found error will be picked up by the first check
// and save verbosity for the second check

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/spec"

	"gitlab.oit.duke.edu/colab/api-colab/libs/datacontract"
	"gitlab.oit.duke.edu/colab/api-colab/libs/oauth"
)

// GetAllClientIDs returns a map of all clientIDs in the database
// using map for O(1) lookup
func GetAllClientIDs() (map[string]string, error) {
	clientIDs := make(map[string]string)
	var apps []datacontract.ApplicationForDB
	tx := db.Begin()
	cursor := tx.Find(&apps)
	if cursor.RecordNotFound() {
		tx.Rollback()
		return clientIDs, nil
	}
	if cursor.Error != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += cursor.Error.Error()
		return nil, e
	}
	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return nil, e
	}
	for _, app := range apps {
		clientIDs[app.ClientID] = ""
	}
	return clientIDs, nil
}

// GetAllDocs returns a map of Swagger Docs in the database
func GetAllDocs() (map[string]map[string]json.RawMessage, error) {
	var apis []datacontract.API
	tx := db.Begin()
	cursor := tx.Find(&apis)
	if cursor.RecordNotFound() {
		tx.Rollback()
		return map[string]map[string]json.RawMessage{}, nil
	}
	if cursor.Error != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += cursor.Error.Error()
		return nil, e
	}
	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return nil, e
	}
	swaggerDocs := make(map[string]map[string]json.RawMessage)
	for _, api := range apis {
		if _, ok := swaggerDocs[api.XName]; !ok {
			swaggerDocs[api.XName] = make(map[string]json.RawMessage)
		}
		swaggerDocs[api.XName][api.Version] = api.SwaggerDocRaw
	}
	return swaggerDocs, nil
}

// GetAPIs returns a map of APIs and their version in the database
func GetAPIs() (map[string][]string, error) {
	var apis []datacontract.API
	tx := db.Begin()
	cursor := tx.Find(&apis)
	if cursor.RecordNotFound() {
		tx.Rollback()
		return nil, nil
	}
	if cursor.Error != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += cursor.Error.Error()
		return nil, e
	}
	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return nil, e
	}
	result := make(map[string][]string)
	for _, api := range apis {
		_, ok := result[api.XName]
		if !ok {
			// XXX: assume capacity 5 to avoid extensive memory manipulation. This number may need to increase
			result[api.XName] = make([]string, 0, 5)
		}
		result[api.XName] = append(result[api.Host], api.Version)
	}
	return result, nil
}

// GetParticularAPI returns Swagger specification for a particular API in the database
func GetParticularAPI(xName string, version string) (json.RawMessage, error) {
	tx := db.Begin()
	var apiInDB datacontract.API
	cursor := tx.Where("x_name = ? AND version = ?", xName, version).First(&apiInDB)
	if cursor.RecordNotFound() {
		if err := tx.Commit().Error; err != nil {
			tx.Rollback()
			e := datacontract.DataBaseError
			e.ErrorMessage += err.Error()
			return nil, e
		}
		return nil, datacontract.NotFoundError
	}
	if cursor.Error != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += cursor.Error.Error()
		return nil, e
	}
	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return nil, e
	}
	return apiInDB.SwaggerDocRaw, nil
}

// AddOrUpdateAPI saves a valid Swagger specification into the database
// It checks for existence to determine whether it is an update or addition
func AddOrUpdateAPI(doc *loads.Document, owner string) error {
	tx := db.Begin()
	// The existence assumption of xName and apiVersion is made because caller handles existence
	xName, _ := doc.Spec().Info.Extensions.GetString("x-name")
	apiVersion := doc.Spec().Info.Version
	var apiInDB datacontract.API
	oldAPICursor := tx.Where("x_name = ? AND version = ? AND host = ?", xName, apiVersion, doc.Host()).First(&apiInDB)
	if oldAPICursor.Error != nil && oldAPICursor.Error.Error() != datacontract.DBRecordNotFoundErrorMessage {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += oldAPICursor.Error.Error()
		return e
	}
	// The following rewrites are used to ensure validity of stored API documents
	// Rewrite schemes
	doc.Spec().Schemes = []string{"https"}
	// Rewrite security definitions
	secDefMap := doc.Spec().SecurityDefinitions
	secDefMap["api_key"] = &spec.SecurityScheme{
		SecuritySchemeProps: spec.SecuritySchemeProps{
			Type: "apiKey",
			Name: "api_key",
			In:   "header",
		},
	}
	dukeAuth, ok := secDefMap["duke_auth"]
	if ok {
		dukeAuth.Type = "oauth2"
		dukeAuth.AuthorizationURL = oauth.OAuthAuthorizationURI
		dukeAuth.Flow = "implicit"
	} else {
		dukeAuth = &spec.SecurityScheme{
			SecuritySchemeProps: spec.SecuritySchemeProps{
				Type:             "oauth2",
				AuthorizationURL: oauth.OAuthAuthorizationURI,
				Flow:             "implicit",
				// TODO: figure out why no scope provided == have meta:api:write as default
				// Scopes: map[string]string{
				// 	"meta:api:write": "Modify your APIs",
				// },
			},
		}
	}
	secDefMap["duke_auth"] = dukeAuth
	doc.Spec().SecurityDefinitions = secDefMap
	// Rewrite security requirement
	secReq := doc.Spec().Security
	found := false
	for _, subMap := range secReq {
		if _, ok = subMap["api_key"]; ok {
			found = true
			subMap["api_key"] = []string{}
		}
	}
	if !found {
		_ = append(secReq, map[string][]string{
			"api_key": []string{},
		})
	}

	// New API and no error occured in the previous steps
	if oldAPICursor.RecordNotFound() {
		newOwners := []datacontract.APIOwner{datacontract.APIOwner{EPPN: owner}}
		newAPI := datacontract.API{
			XName:         xName,
			Host:          doc.Host(),
			Version:       apiVersion,
			SwaggerDocRaw: doc.Raw(),
			Owners:        newOwners,
		}
		// ensure owner exists
		for _, o := range newOwners {
			if err := tx.FirstOrCreate(&o).Error; err != nil {
				tx.Rollback()
				return err
			}
		}
		// create new API
		if err := tx.Create(&newAPI).Error; err != nil {
			tx.Rollback()
			return err
		}

		if err := tx.Commit().Error; err != nil {
			tx.Rollback()
			return err
		}
		return nil
	}
	// Update API
	// Confirm ownership
	isOwnerOf := false
	// Query for Owners
	var apiOwners []datacontract.APIOwner
	ownerCursor := tx.Model(&apiInDB).Related(&apiOwners, "Owners")
	if ownerCursor.Error != nil {
		e := datacontract.DataBaseError
		e.ErrorMessage += ownerCursor.Error.Error()
		return e
	}
	for _, v := range apiOwners {
		if v.EPPN == owner {
			isOwnerOf = true
			break
		}
	}
	if !isOwnerOf {
		tx.Rollback()
		return datacontract.NotOwnerError
	}
	apiInDB.XName = xName
	apiInDB.Host = doc.Host()
	apiInDB.Version = apiVersion
	apiInDB.SwaggerDocRaw = doc.Raw()
	updatedAPICursor := tx.Save(&apiInDB)
	if updatedAPICursor.Error != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += updatedAPICursor.Error.Error()
		return e
	}
	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return e
	}
	return nil
}

// RemoveAPI deletes a valid Swagger specification from the database
func RemoveAPI(doc *loads.Document, owner string) error {
	tx := db.Begin()
	// The existence assumption of xName and apiVersion is made because caller handles existence
	xName, _ := doc.Spec().Info.Extensions.GetString("x-name")
	apiVersion := doc.Spec().Info.Version
	var apiInDB datacontract.API
	staleAPICursor := tx.Where("x_name = ? AND version = ? AND host = ?", xName, apiVersion, doc.Host()).First(&apiInDB)
	if staleAPICursor.RecordNotFound() {
		tx.Rollback()
		return datacontract.APINotExistError
	}
	if staleAPICursor.Error != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += staleAPICursor.Error.Error()
		return e
	}
	// Confirm ownership
	isOwnerOf := false
	// Query for Owners
	var apiOwners []datacontract.APIOwner
	ownerCursor := tx.Model(&apiInDB).Related(&apiOwners, "Owners")
	if ownerCursor.Error != nil {
		e := datacontract.DataBaseError
		e.ErrorMessage += ownerCursor.Error.Error()
		return e
	}
	for _, v := range apiOwners {
		if v.EPPN == owner {
			isOwnerOf = true
			break
		}
	}
	if !isOwnerOf {
		tx.Rollback()
		return datacontract.NotOwnerError
	}

	if err := tx.Delete(&apiInDB).Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return e
	}
	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return e
	}
	return nil
}

// GetAppsAsUser returns a detailed list of application user owns.
// If no app owned by user, return an empty array instead of nil
func GetAppsAsUser(user string) ([]datacontract.ApplicationForHTTP, error) {
	tx := db.Begin()
	// Query for application owner first
	var owner datacontract.AppOwner
	ownerCursor := tx.Where("eppn = ?", user).First(&owner)
	if ownerCursor.RecordNotFound() {
		if err := tx.Commit().Error; err != nil {
			e := datacontract.DataBaseError
			e.ErrorMessage += err.Error()
			return nil, e
		}
		return []datacontract.ApplicationForHTTP{}, nil
	}
	if ownerCursor.Error != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += ownerCursor.Error.Error()
		return nil, e
	}
	// Query for applications owned by owner
	var appList []datacontract.ApplicationForDB
	appListCursor := tx.Model(&owner).Related(&appList, "Applications")
	if appListCursor.Error != nil && appListCursor.Error.Error() != datacontract.DBRecordNotFoundErrorMessage {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += appListCursor.Error.Error()
		return nil, e
	}
	// populate owner fields
	for i, v := range appList {
		if err := tx.Model(&v).Related(&appList[i].AppOwners, "AppOwners").Error; err != nil {
			e := datacontract.DataBaseError
			e.ErrorMessage += err.Error()
			return nil, e
		}
	}
	// Convert
	resultList := make([]datacontract.ApplicationForHTTP, len(appList))
	for i, v := range appList {
		forHTTP, err := prepareAppForHTTP(v)
		if err != nil {
			e := datacontract.SystemError
			e.ErrorMessage = err.Error()
			return nil, e
		}
		resultList[i] = *forHTTP
	}
	if err := tx.Commit().Error; err != nil {
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return nil, e
	}
	return resultList, nil
}

// GetAppsAsAdmin returns a full detailed list of applications in the database
func GetAppsAsAdmin() {
	return
}

// AddOrUpdateApp saves a valid application JSON specification into the database provided it is confirmed by upstream OAuth server
// It checks for existence to determine whether it is an update or addition
func AddOrUpdateApp(app datacontract.ApplicationForHTTP, owner string, oAuthToken string) (*datacontract.ApplicationForHTTP, error) {
	// Differentiate between update and addition
	tx := db.Begin()
	var appInDB datacontract.ApplicationForDB
	oldAppCursor := tx.Where("client_id = ?", app.ClientID).First(&appInDB)
	if oldAppCursor.Error != nil && oldAppCursor.Error.Error() != datacontract.DBRecordNotFoundErrorMessage {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += oldAppCursor.Error.Error()
		return nil, e
	}
	// Differentiate which way (register/update) to forward to OAuth
	// Prepare var for shared OAuth responses and error
	var resp *http.Response
	var oAuthErr error
	// Register App to OAuth
	if oldAppCursor.RecordNotFound() {
		// forward to OAuth for registration
		resp, oAuthErr = oauth.ForwardToOAuth(app, owner, oAuthToken, oauth.OAuthRegisterAppURI)
		if oAuthErr != nil {
			tx.Rollback()
			e := datacontract.OAuthUnexpectedError
			e.ErrorMessage = oAuthErr.Error()
			return nil, e
		}
	} else { // Update App to OAuth
		resp, oAuthErr = oauth.ForwardToOAuth(app, owner, oAuthToken, oauth.OAuthUpdateAppURI)
		if oAuthErr != nil {
			tx.Rollback()
			e := datacontract.OAuthUnexpectedError
			e.ErrorMessage = oAuthErr.Error()
			return nil, e
		}
	}

	// Process OAuth response
	defer resp.Body.Close()
	respBody, bodyErr := ioutil.ReadAll(resp.Body)
	if bodyErr != nil {
		tx.Rollback()
		e := datacontract.OAuthUnexpectedError
		e.ErrorMessage = bodyErr.Error()
		return nil, e
	}
	// Only proceed with a 200 OK. Everything else is thrown as 502 Bad Gateway.
	switch resp.StatusCode {
	default:
		tx.Rollback()
		e := datacontract.OAuthUnexpectedError
		e.ErrorMessage = resp.Status
		return nil, e
	case 409:
		tx.Rollback()
		var oauthErr struct {
			Error string `json:"error"`
		}
		if formatErr := json.Unmarshal(respBody, &oauthErr); formatErr != nil {
			e := datacontract.SystemError
			e.ErrorMessage = formatErr.Error()
			return nil, e
		}
		e := datacontract.OAuthBadRequestError
		e.ErrorMessage += oauthErr.Error
		return nil, e
	case 200:
	}
	// Unmarshalling the response to a struct
	var confirmedApp datacontract.ApplicationForHTTP
	if err := json.Unmarshal(respBody, &confirmedApp); err != nil {
		tx.Rollback()
		e := datacontract.SystemError
		e.ErrorMessage = err.Error()
		return nil, e
	}
	// Prepare struct for database
	confirmedAppForDB, err := prepareAppForDB(confirmedApp)
	if err != nil {
		tx.Rollback()
		e := datacontract.SystemError
		e.ErrorMessage = err.Error()
		return nil, e
	}

	// Ensure existence of each AppOwner
	for _, appOwnerInDB := range confirmedAppForDB.AppOwners {
		if err := tx.FirstOrCreate(&appOwnerInDB).Error; err != nil {
			tx.Rollback()
			e := datacontract.DataBaseError
			e.ErrorMessage += err.Error()
			return nil, e
		}
	}

	// New App to DB
	if oldAppCursor.RecordNotFound() {
		// NOTE: because the AppOwner owns ApplicationForDB, and initiates the many to many relationship
		// must create AppOwner First
		if err := tx.Create(confirmedAppForDB).Error; err != nil {
			tx.Rollback()
			e := datacontract.DataBaseError
			e.ErrorMessage += err.Error()
			return nil, e
		}
		if err := tx.Commit().Error; err != nil {
			tx.Rollback()
			e := datacontract.DataBaseError
			e.ErrorMessage += err.Error()
			return nil, e
		}
		return &confirmedApp, nil
	}
	// Update App in DB
	// Ownership is verified by OAuth when forwardToOAuth is called with Basic HTTP Auth
	if err := tx.Model(&appInDB).Updates(confirmedAppForDB).Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return nil, e
	}
	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return nil, e
	}
	return &confirmedApp, nil
}

// RemoveApp deletes a valid application JSON specification from the database provided it is confirmed by upstream OAuth server
func RemoveApp(app datacontract.ApplicationForHTTP, owner string, oAuthToken string) error {
	tx := db.Begin()
	// Confirm existence of app
	var appInDB datacontract.ApplicationForDB
	staleAppCursor := tx.Where("client_id = ?", app.ClientID).First(&appInDB)
	if staleAppCursor.RecordNotFound() {
		tx.Rollback()
		return datacontract.AppNotExistError
	}
	if staleAppCursor.Error != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += staleAppCursor.Error.Error()
		return e
	}
	// Forward to OAuth for deletion
	resp, err := oauth.ForwardToOAuth(app, owner, oAuthToken, oauth.OAuthRevokeAppURI)
	if err != nil {
		tx.Rollback()
		e := datacontract.OAuthUnexpectedError
		e.ErrorMessage = err.Error()
		return e
	}
	// Process OAuth response
	defer resp.Body.Close()
	respBody, bodyErr := ioutil.ReadAll(resp.Body)
	if bodyErr != nil {
		tx.Rollback()
		e := datacontract.OAuthUnexpectedError
		e.ErrorMessage = err.Error()
		return e
	}
	// Only proceed with a 200 OK. Everything else is thrown as 502 Bad Gateway.
	switch resp.StatusCode {
	default:
		tx.Rollback()
		e := datacontract.OAuthUnexpectedError
		e.ErrorMessage = resp.Status
		return e
	case 409:
		tx.Rollback()
		var oauthErr struct {
			Error string `json:"error"`
		}
		if formatErr := json.Unmarshal(respBody, &oauthErr); formatErr != nil {
			e := datacontract.SystemError
			e.ErrorMessage = formatErr.Error()
			return e
		}
		e := datacontract.OAuthBadRequestError
		e.ErrorMessage += oauthErr.Error
		return e
	case 204:
	}

	if err := tx.Delete(&appInDB).Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return e
	}
	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		e := datacontract.DataBaseError
		e.ErrorMessage += err.Error()
		return e
	}
	return nil
}
