package oauth

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/spec"

	"gitlab.oit.duke.edu/colab/api-colab/libs/datacontract"
)

// ForwardToOAuth forwards an incoming application to OAuth for
// registration/revokation/update/validation
func ForwardToOAuth(app datacontract.ApplicationForHTTP, owner string, oAuthToken string, uri string) (*http.Response, error) {
	// Prepare request body
	appInBytes, err := json.Marshal(app)
	if err != nil {
		return nil, err
	}
	// Prepare request
	req, err := http.NewRequest("POST", uri, bytes.NewBuffer(appInBytes))
	if err != nil {
		return nil, err
	}
	// Set HTTP Headers
	req.Header.Set("content-type", "application/json")
	// Prepare HTTP Basic Auth
	req.SetBasicAuth(owner, oAuthToken)
	// Perform request
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

// ValidateScope takes path, HTTP operation and OAuth token and handles scope validation
// by getting allowed scopes from the token and compare against required scopes for the endpoint
// It returns a valid eppn, any error
func ValidateScope(oAuthToken string, relativePath string, operation string, metaSwaggerDocument *loads.Document) (string, error) {
	// Get allowed scopes and eppn
	eppn, scopesMap, err := getAllowedScopeAndEppn(oAuthToken)
	if err != nil {
		if err.Error() == "invalid_token" || err.Error() == "expired_token" {
			e := datacontract.InvalidTokenError
			e.ErrorMessage = err.Error()
			return "", e
		}
		e := datacontract.OAuthUnexpectedError
		e.ErrorMessage += err.Error()
		return "", e
	}
	// Get required scopes to touch relativePath
	pathItem, ok := metaSwaggerDocument.Spec().Paths.Paths[relativePath]
	if !ok {
		e := datacontract.SwaggerPathError
		e.ErrorMessage = fmt.Sprintf("cannot find %s with %s in metaAPI", relativePath, operation)
		return "", e
	}
	var opInPathItem *spec.Operation
	switch operation {
	default:
		e := datacontract.SwaggerOperationNotExistError
		e.ErrorMessage = fmt.Sprintf("%s at %s is not implemented", operation, relativePath)
		return "", e
	case http.MethodGet:
		opInPathItem = pathItem.Get
	case http.MethodPost:
		opInPathItem = pathItem.Post
	case http.MethodDelete:
		opInPathItem = pathItem.Delete
	}
	var securityRequirements []string
	for _, subMap := range opInPathItem.Security {
		if secReq, ok := subMap["duke_auth"]; ok {
			securityRequirements = secReq
			break
		}
	}
	if securityRequirements == nil {
		e := datacontract.SwaggerSecRequireNotExistError
		e.ErrorMessage = "required scopes do not exist"
		return "", e
	}
	for _, secReq := range securityRequirements {
		if scope, ok := scopesMap[secReq]; !ok {
			e := datacontract.TokenScopeError
			e.ErrorMessage += scope
			return "", e
		}
	}
	return eppn, nil
}

// ValidateExpiration takes the token to OAuth system
// and makes sure it is a good token
func ValidateExpiration(oAuthToken string) (string, error) {
	s, _, e := getAllowedScopeAndEppn(oAuthToken)
	if e.Error() == "invalid_token" || e.Error() == "expired_token" {
		eInternal := datacontract.InvalidTokenError
		eInternal.ErrorMessage = e.Error()
		return s, eInternal
	}
	eInternal := datacontract.OAuthUnexpectedError
	eInternal.ErrorMessage += e.Error()
	return s, eInternal
}

// getAllowedScopeAndEppn validates OAuth Token and returns allowed scope and eppn
// return map for O(1) lookup
func getAllowedScopeAndEppn(oAuthToken string) (string, map[string]string, error) {
	uri := fmt.Sprintf("%s?access_token=%s", OAuthValidationURI, oAuthToken)
	resp, err := http.Get(uri)
	if err != nil {
		return "", nil, err
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return "", nil, err
	}
	var jsonResp map[string]interface{}
	if err = json.Unmarshal(respBody, &jsonResp); err != nil {
		return "", nil, err
	}
	errStr, ok := jsonResp["error"]
	if ok {
		return "", nil, errors.New(errStr.(string))
	}
	eppn, ok := jsonResp["eppn"]
	if !ok {
		return "", nil, errors.New("invalid response, no eppn")
	}
	scopeStr, ok := jsonResp["scope"]
	if !ok {
		return "", nil, errors.New("invalid response, no scope")
	}
	components := strings.Split(scopeStr.(string), " ")
	resultMap := make(map[string]string)
	for _, v := range components {
		resultMap[v] = ""
	}
	return eppn.(string), resultMap, nil
}
