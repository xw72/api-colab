package main

// If you have question about whether to use pointers or values in Go,
// please look at http://stackoverflow.com/questions/23542989/pointers-vs-values-in-parameters-and-return-values first.

// NOTE: About OAuth app:
// To create, no secret is needed. To update and delete, arbitrary secret is needed (client secret property/field need to have non null value)

// TODO: ensure secdef, scheme, etc (refer to Typescript def) exist before add or update API
// TODO: rate limiter

import (
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/validate"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	"gitlab.oit.duke.edu/colab/api-colab/libs/database"
	"gitlab.oit.duke.edu/colab/api-colab/libs/datacontract"
)

func main() {
	// Start default logger
	GenericLogger = log.New(os.Stdout, "[API.Colab Server] ", log.LstdFlags)
	GenericLogger.Println("Logger initialised.")

	// Parse env vars
	ParseEnvVar()

	// Parse private key for signing signature
	GenericLogger.Println("Loading server private key...")
	ParsePrivateKeyFromFile("./ssl/api-colab-duke-edu.key")
	GenericLogger.Println("Server private key loaded.")

	// Initialise database
	GenericLogger.Println("Initialising database...")
	err := database.InitAndConnect(DBHost, DBUser, DBPassword, DBName, VerboseDBLoggingOverride, os.Stdout)
	if err != nil {
		panic("Database initialization error: " + err.Error())
	}
	GenericLogger.Println("Database is initialised.")
	defer database.Close()

	// Parse Swagger specification
	GenericLogger.Println("Parsing Swagger specification...")
	ParseSpec()
	GenericLogger.Println("Swagger specification is loaded.")

	// Start server based on GoEnv
	// According to the documentation of golang net/http,
	// each incoming request is handled by a separate goroutine.
	// Therefore, even if one handler blocks, the server will not stop serving other requests.
	// https://golang.org/pkg/net/http/#Serve
	switch datacontract.GoEnv {
	default:
		GenericLogger.Println("No environment specified. Quitting...")
	case "development":
		GenericLogger.Println("Development server starting, listening on 3001")
		StartServer(":3001", "./ssl/snakeoil.cert", "./ssl/snakeoil.key", ConfigureRoutingAndMiddleware())
	case "production":
		GenericLogger.Println("Production server starting, listening on 443")
		StartServer(":443", "./ssl/api-colab-duke-edu-concat.cert", "./ssl/api-colab-duke-edu.key", ConfigureRoutingAndMiddleware())
	}
}

func StartServer(uri string, certificatePath string, keyPath string, router http.Handler) {
	http.ListenAndServeTLS(uri, "./ssl/snakeoil.cert", "./ssl/snakeoil.key", router)
}

func ConfigureRoutingAndMiddleware() http.Handler {
	// Configure routing
	r := mux.NewRouter()
	// /meta subrouter
	rMetaV1 := r.PathPrefix("/meta/v1/").Subrouter()
	rMetaV1.HandleFunc("/docs", DocsHandler)
	rMetaV1.HandleFunc("/apis", APIsHandler)
	rMetaV1.HandleFunc("/apis/{api}/{version}", SpecificAPIHandler)
	rMetaV1.HandleFunc("/apps", AppsHandler)
	rMetaV1.HandleFunc("/status", StatusHandler)
	// subAPI router
	r.HandleFunc("/{apiName}/{version}/{path:.*}", SubAPIHandler)
	// 404 router
	r.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, request *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	})

	// Configure Middleware
	var router http.Handler
	router = r
	router = ForceJSONHandler(router)
	if TimedLoggingOverride {
		TimedLogger = log.New(os.Stdout, "[API.Colab Time] ", log.LstdFlags)
		router = TimedLoggingHandler(router)
	}
	router = handlers.CombinedLoggingHandler(os.Stdout, router)
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "X-API-Key", "Content-Type", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "OPTIONS", "DELETE"})
	router = handlers.CORS(headersOk, originsOk, methodsOk)(router)
	if datacontract.GoEnv == "production" {
		router = handlers.RecoveryHandler()(router)
	} else {
		router = handlers.RecoveryHandler(handlers.PrintRecoveryStack((true)))(router)
	}
	router = AuthenticationHandler(router)
	return router
}

// ParseEnvVar parses any environment variables and assign them to global variables
func ParseEnvVar() {
	for _, e := range os.Environ() {
		pair := strings.Split(e, "=")
		switch pair[0] {
		case "GOENV":
			switch pair[1] {
			case "production":
				datacontract.GoEnv = pair[1]
			case "staging":
				datacontract.GoEnv = pair[1]
			case "development":
				datacontract.GoEnv = pair[1]
			}
		case "AUTH_OVERRIDE":
			switch strings.ToLower(pair[1]) {
			default:
				AuthenticationOverride = false
			case "1", "true":
				GenericLogger.Println("Authentication override enabled")
				AuthenticationOverride = true
			}
		case "TIMED_LOG_OVERRIDE":
			switch strings.ToLower(pair[1]) {
			default:
				TimedLoggingOverride = false
			case "1", "true":
				GenericLogger.Println("Timed logging override enabled")
				TimedLoggingOverride = true
			}
		case "VERBOSE_DB_OVERRIDE":
			switch strings.ToLower(pair[1]) {
			default:
				VerboseDBLoggingOverride = false
			case "1", "true":
				GenericLogger.Println("Verbose database logging override enabled")
				VerboseDBLoggingOverride = true
			}
		}
	}
}

// ParseSpec parses the Swagger specification to map[string]interface{}
func ParseSpec() {
	var err error
	MetaSwaggerDocument, err = loads.JSONSpec("./swagger-specs/metaAPI.json")
	if err != nil {
		panic("cannot read metaAPI Swagger specification " + err.Error())
	}
	if err = validate.Spec(MetaSwaggerDocument, strfmt.Default); err != nil {
		panic("metaAPI Swagger specification does not validate against Swagger 2.0 specification")
	}
}

// ParsePrivateKeyFromFile parses private key from an RSA PEM encoded file
func ParsePrivateKeyFromFile(path string) {
	privKeyFile, err := ioutil.ReadFile(path)
	if err != nil {
		panic("Cannot open private key file. " + err.Error())
	}
	p, _ := pem.Decode(privKeyFile)
	privKeyStruct, err := x509.ParsePKCS1PrivateKey(p.Bytes)
	if err != nil {
		panic("Cannot parse private key file. " + err.Error())
	}
	PrivateKey = privKeyStruct
}
