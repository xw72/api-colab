package main

import (
	"encoding/json"
	"net/http"
	"strings"

	"gitlab.oit.duke.edu/colab/api-colab/libs/database"
	"gitlab.oit.duke.edu/colab/api-colab/libs/datacontract"
)

type authenticationHandler struct {
	handler http.Handler
}

// AuthenticationHandler handles authentication for incoming requests.
// It is meant to be used as a middleware.
func AuthenticationHandler(h http.Handler) http.Handler {
	return authenticationHandler{handler: h}
}

func (auth authenticationHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// If authentication override is enabled and the application is not ra
	// in production mode, skip authentication.
	if datacontract.GoEnv != "producion" && AuthenticationOverride {
		auth.handler.ServeHTTP(w, req)
		return
	}
	if req.Method == http.MethodOptions && req.Header.Get("access-control-request-method") != "" {
		auth.handler.ServeHTTP(w, req)
		return
	}
	// Look for x-api-key
	apiKey := req.Header.Get("x-api-key")
	if apiKey == "" {
		e := datacontract.RequestNoXAPIKeyHeaderError
		w.WriteHeader(e.HTTPStatusCode)
		json.NewEncoder(w).Encode(e)
		return
	}
	// Confirm validity for x-api-key
	apiKeys, err := database.GetAllClientIDs()
	if err != nil {
		if e, ok := err.(datacontract.InternalError); ok {
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
		} else {
			e := datacontract.InternalLogicsError
			e.ErrorMessage += err.Error()
			w.WriteHeader(e.HTTPStatusCode)
			json.NewEncoder(w).Encode(e)
		}
		return
	}
	if _, ok := apiKeys[apiKey]; !ok {
		e := datacontract.RequestInvalidXAPIKeyError
		w.WriteHeader(e.HTTPStatusCode)
		json.NewEncoder(w).Encode(e)
		return
	}
	// NOTE: Search for OAuth tokens is delayed to specific handler called on that path
	// Serve request
	auth.handler.ServeHTTP(w, req)
}

// extractOAuthToken extracts OAuth token from incoming HTTP requests
func extractOAuthToken(r *http.Request) (string, error) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return "", datacontract.RequestNoAuthorizationHeaderError
	}
	components := strings.Split(authHeader, " ")
	if components[0] != "Bearer" || components[1] == "" {
		return "", datacontract.RequestAuthorizationHeaderFormatError
	}
	return components[1], nil
}
