package database

import (
	"encoding/json"

	"gitlab.oit.duke.edu/colab/api-colab/libs/datacontract"
)

// prepareAppForDB converts HTTP app struct to DB app struct
// returns nil and error if cannot convert
// returns default app with ApplicationForDB{} contains default value
// and is invalid so makes more sense just return nil
func prepareAppForDB(app datacontract.ApplicationForHTTP) (*datacontract.ApplicationForDB, error) {
	// RedirectURIs and Permissions are stored as blob in database
	// because []string cannot be saved into a column
	newOwners := make([]datacontract.AppOwner, len(app.AppOwners))
	for i, v := range app.AppOwners {
		newOwners[i] = datacontract.AppOwner{EPPN: v}
	}
	redirectURIs, err := json.Marshal(app.RedirectURIs)
	if err != nil {
		return nil, err
	}
	permissions, err := json.Marshal(app.Permissions)
	if err != nil {
		return nil, err
	}
	appDocRaw, err := json.Marshal(app)
	if err != nil {
		return nil, err
	}
	return &datacontract.ApplicationForDB{
		ClientID:         app.ClientID,
		ClientSecret:     app.ClientSecret,
		RedirectURIs:     redirectURIs,
		DisplayName:      app.DisplayName,
		Description:      app.Description,
		OwnerDescription: app.OwnerDescription,
		PrivacyURL:       app.PrivacyURL,
		Permissions:      permissions,
		AppOwners:        newOwners,
		AppDocRaw:        appDocRaw,
	}, nil
}

// prepareAppForHTTP converts DB app struct to HTTP app struct
// returns nil and error if cannot convert
// returns default app with ApplicationForHTTP{} contains default value
// and is invalid so makes more sense just return nil
func prepareAppForHTTP(app datacontract.ApplicationForDB) (*datacontract.ApplicationForHTTP, error) {
	// convert DB owners to HTTP owner array
	ownersList := make([]string, len(app.AppOwners))
	for i, v := range app.AppOwners {
		ownersList[i] = v.EPPN
	}

	// convert DB redirect URIs to HTTP redirect URIs
	var redirectURIs []string
	if err := json.Unmarshal(app.RedirectURIs, &redirectURIs); err != nil {
		return nil, err
	}
	var permissions []datacontract.Permission
	if err := json.Unmarshal(app.Permissions, &permissions); err != nil {
		return nil, err
	}
	return &datacontract.ApplicationForHTTP{
		ClientID:         app.ClientID,
		ClientSecret:     app.ClientSecret,
		RedirectURIs:     redirectURIs,
		DisplayName:      app.DisplayName,
		Description:      app.Description,
		OwnerDescription: app.OwnerDescription,
		PrivacyURL:       app.PrivacyURL,
		Permissions:      permissions,
		AppOwners:        ownersList,
	}, nil
}
