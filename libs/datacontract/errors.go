package datacontract

import (
	"net/http"
)

const (
	DBRecordNotFoundErrorMessage = "record not found"
)

// Error is a generic error
// It can be serialized into json HTTP response
type InternalError struct {
	ErrorCode      int    `json:"errorCode"`
	ErrorMessage   string `json:"errorMessage"`
	HTTPStatusCode int    `json:"-"`
}

func (e InternalError) Error() string {
	return e.ErrorMessage
}

// NOTE: because go assignment always makes a copy
// unless the pointer is passed, we can reuse the structs below
var (
	NotOwnerError = InternalError{
		ErrorCode:      400101,
		ErrorMessage:   "The person initiating request does not own this API",
		HTTPStatusCode: http.StatusBadRequest,
	}
	APINotExistError = InternalError{
		ErrorCode:      400102,
		ErrorMessage:   "The API to be deleted does not exist",
		HTTPStatusCode: http.StatusBadRequest,
	}
	AppNotExistError = InternalError{
		ErrorCode:      400103,
		ErrorMessage:   "The application to be deleted does not exist",
		HTTPStatusCode: http.StatusBadRequest,
	}
	OAuthBadRequestError = InternalError{
		ErrorCode:      400104,
		ErrorMessage:   "[409] ",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestNoAuthorizationHeaderError = InternalError{
		ErrorCode:      400105,
		ErrorMessage:   "no authorization header provided",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestAuthorizationHeaderFormatError = InternalError{
		ErrorCode:      400106,
		ErrorMessage:   "ill-formatted authorization header",
		HTTPStatusCode: http.StatusBadRequest,
	}
	InvalidTokenError = InternalError{
		ErrorCode:      400107,
		ErrorMessage:   "",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestNoBodyError = InternalError{
		ErrorCode:      400108,
		ErrorMessage:   "cannot read request body",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestBodyToSwaggerError = InternalError{
		ErrorCode:      400109,
		ErrorMessage:   "cannot parse request body to Swagger specification ",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestBodySwaggerValidationError = InternalError{
		ErrorCode:      400110,
		ErrorMessage:   "Request body does not validate against json schema for Swagger. ",
		HTTPStatusCode: http.StatusBadRequest,
	}
	TokenScopeError = InternalError{
		ErrorCode:      400111,
		ErrorMessage:   "token is not authorized for required scope ",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestBodySwaggerNoXNameError = InternalError{
		ErrorCode:      400112,
		ErrorMessage:   "cannot read x-name from request body",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestBodySwaggerXNameFormatError = InternalError{
		ErrorCode:      400113,
		ErrorMessage:   "ill-formatted x-name field, only alphanumerics, underscores, and dashes are allowed",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestBodySwaggerNoVersionError = InternalError{
		ErrorCode:      400114,
		ErrorMessage:   "no version present in info property of request body",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestBodySwaggerVersionFormatError = InternalError{
		ErrorCode:      400115,
		ErrorMessage:   "ill-formatted version field, only alphanumerics, underscores, and dashes are allowed",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestBodySwaggerNoHostError = InternalError{
		ErrorCode:      400116,
		ErrorMessage:   "no host present",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestBodySwaggerNoSecDefError = InternalError{
		ErrorCode:      400117,
		ErrorMessage:   "No security definitions exist",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestBodySwaggerNoSchemesError = InternalError{
		ErrorCode:      400118,
		ErrorMessage:   "No schemes exist",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestBodySwaggerSchemesMismatchError = InternalError{
		ErrorCode:      400119,
		ErrorMessage:   "multiple schemes or not https scheme",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestAppBodyParseError = InternalError{
		ErrorCode:      400120,
		ErrorMessage:   "cannot parse request body ",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestAppBodyClientIDFormatError = InternalError{
		ErrorCode:      400121,
		ErrorMessage:   "ill-formatted client id field, only alphanumerics, underscores, and dashes are allowed",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestAppBodyRedirectURIFormatError = InternalError{
		ErrorCode:      400122,
		ErrorMessage:   "cannot parse redirect URI due to ",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestAppBodyPrivacyURIFormatError = InternalError{
		ErrorCode:      400123,
		ErrorMessage:   "cannot parse privacy URL due to ",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestNoXAPIKeyHeaderError = InternalError{
		ErrorCode:      400124,
		ErrorMessage:   "No X-API-Key is present in request header",
		HTTPStatusCode: http.StatusBadRequest,
	}
	RequestInvalidXAPIKeyError = InternalError{
		ErrorCode:      400125,
		ErrorMessage:   "invalid X-API-Key",
		HTTPStatusCode: http.StatusBadRequest,
	}

	NotFoundError = InternalError{
		ErrorCode:      404101,
		ErrorMessage:   DBRecordNotFoundErrorMessage,
		HTTPStatusCode: http.StatusNotFound,
	}

	DataBaseError = InternalError{
		ErrorCode:      500101,
		ErrorMessage:   "Database Error: ",
		HTTPStatusCode: http.StatusInternalServerError,
	}
	SystemError = InternalError{
		ErrorCode:      500102,
		ErrorMessage:   "system error",
		HTTPStatusCode: http.StatusInternalServerError,
	}
	InternalLogicsError = InternalError{
		ErrorCode:      500103,
		ErrorMessage:   "Internal Server Error. Please report this incident at https://gitlab.oit.duke.edu/colab/api-colab/issues." + " ",
		HTTPStatusCode: http.StatusInternalServerError,
	}
	SwaggerPathError = InternalError{
		ErrorCode:      500104,
		ErrorMessage:   "path error",
		HTTPStatusCode: http.StatusInternalServerError,
	}
	SwaggerOperationNotExistError = InternalError{
		ErrorCode:      500105,
		ErrorMessage:   "no ops error",
		HTTPStatusCode: http.StatusInternalServerError,
	}
	SwaggerSecRequireNotExistError = InternalError{
		ErrorCode:      500106,
		ErrorMessage:   "not security requirement",
		HTTPStatusCode: http.StatusInternalServerError,
	}

	OAuthUnexpectedError = InternalError{
		ErrorCode:      502101,
		ErrorMessage:   "[502] unexpected upstream answer: ",
		HTTPStatusCode: http.StatusBadGateway,
	}
	SubServiceUnavailableError = InternalError{
		ErrorCode:      502102,
		ErrorMessage:   "subservice is temporarily unavailable",
		HTTPStatusCode: http.StatusBadGateway,
	}
)
