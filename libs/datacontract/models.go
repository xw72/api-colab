package datacontract

import (
	"time"
)

// Model is an override of gorm.Model which hides fields except for ID
type Model struct {
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}

// Admin is the DB schema for admins
type Admin struct {
	Model
	NetID string
}

// API is the DB schema for APIs
type API struct {
	Model         `json:"-"`
	ID            uint `gorm:"primary_key"`
	XName         string
	Host          string
	Version       string
	SwaggerDocRaw []byte     `gorm:"type:bytea"`
	Owners        []APIOwner `gorm:"many2many:api_owners_apis;"`
}

// APIOwner describes owner of resources in the system
type APIOwner struct {
	Model
	APIs []API  `gorm:"many2many:api_owners_apis;"`
	EPPN string `gorm:"primary_key"`
}

// ApplicationForHTTP is meant to be used for HTTP request/response serialization
// WARNING: This structure CANNOT be used to save in database
type ApplicationForHTTP struct {
	ClientID         string       `json:"clientId"`
	ClientSecret     string       `json:"clientSecret"`
	RedirectURIs     []string     `json:"redirectURIs"`
	DisplayName      string       `json:"displayName"`
	Description      string       `json:"description"`
	OwnerDescription string       `json:"ownerDescription"`
	PrivacyURL       string       `json:"privacyURL"`
	Permissions      []Permission `json:"permissions"`
	AppOwners        []string     `json:"appOwners"`
}

// Permission describes permission application may request to have
// Access can be one of "full" | "pseudo" | "none"
type Permission struct {
	Service string `json:"service"`
	Access  string `json:"access"`
}

// ApplicationForDB is the DB schema for apps
// WARNING: This sturcture CANNOT be directly marshaled by encoding/json for HTTP response body
// Note: The AppOwner field is a struct, but the HTTP response body is []string
// So before marshaling for HTTP response body, process the data.
type ApplicationForDB struct {
	Model
	ID               uint   `gorm:"primary_key"`
	ClientID         string `gorm:"primary_key"`
	ClientSecret     string
	RedirectURIs     []byte `gorm:"type:bytea"`
	DisplayName      string
	Description      string
	OwnerDescription string
	PrivacyURL       string
	Permissions      []byte     `gorm:"type:bytea"`
	AppOwners        []AppOwner `gorm:"many2many:app_owners_application_for_dbs;"`
	AppDocRaw        []byte     `gorm:"type:bytea"`
}

// AppOwner describes owner of resources in the system
type AppOwner struct {
	Model
	Applications []ApplicationForDB `gorm:"many2many:app_owners_application_for_dbs;"`
	EPPN         string             `gorm:"primary_key"`
}
