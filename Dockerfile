FROM golang:1.8

ENV GOENV=production

RUN curl https://glide.sh/get | sh

WORKDIR /go/src/api-colab

COPY ./glide.yaml ./glide.lock ./

RUN glide install

COPY ./ssl ./ssl

COPY ./swagger-specs ./swagger-specs

COPY ./*.go ./

RUN go install

CMD $GOPATH/bin/api-colab

EXPOSE 443
