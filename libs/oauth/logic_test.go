package oauth

import (
	"httptest"
	"testing"
)

var (
	goodOauthServerEmulator = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.Write([]byte(`{
			"eppn":  "abcd@duke.edu",
			"scope": "meta:api:write meta:apps:write meta:apps:read oauth_registrations identity:netid:read"
    }`))
	}))

	errorOauthServerEmulator = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.Write([]byte(`{
			"error":            "expired_token",
			"error_description": "The access token provided has expired"
    }`))
	}))

	insufficientScopeServerEmulator = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.Write([]byte(`{
			"eppn":  "abcd@duke.edu",
			"scope": "basic"
    }`))
	}))

	testServer = httptest.NewServer(ConfigureRoutingAndMiddleware())
)

func TestMain(m *testing.M) {
	DBHost = "127.0.0.1"
	DBName = "api-colab-test"
	DBUser = "test"
	DBPassword = "testpass"
	database.InitAndConnect(DBHost, DBUser, DBPassword, DBName, false, os.Stdout)
	testApp := datacontract.ApplicationForDB{
		ClientID:         "test",
		ClientSecret:     "somesecret",
		RedirectURIs:     []byte(`["http://localhost:3000/"]`),
		DisplayName:      "test-displayName",
		Description:      "test-desc",
		OwnerDescription: "test-owner",
		PrivacyURL:       "http://localhost:3000/privacy",
		Permissions:      []byte(`{"service": "basic", "access": "full"}`),
		AppOwners: []datacontract.AppOwner{
			datacontract.AppOwner{EPPN: "test@duke.edu"},
		},
	}
	database.Seed(&testApp)
	defer database.Close()
	ParseSpec()
	ParsePrivateKeyFromFile("./ssl/api-colab-duke-edu.key")
	defer testServer.Close()
	os.Exit(m.Run())
}

func TestRequestOAuthAuthorizationHeader(t *testing.T) {
	t.Run("NoAuthorizationHeaderWhenNotRequired")
}
