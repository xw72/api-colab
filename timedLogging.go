package main

import (
	"net/http"
	"time"
)

type timedLoggingHandler struct {
	handler http.Handler
}

// TimedLoggingHandler logs the time it takes the server to complete each request
func TimedLoggingHandler(h http.Handler) http.Handler {
	return timedLoggingHandler{handler: h}
}

func (t timedLoggingHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	start := time.Now()
	
	t.handler.ServeHTTP(w, req)

	TimedLogger.Printf(
		"%s\t%s\t%s",
		req.Method,
		req.RequestURI,
		time.Since(start),
	)
}
