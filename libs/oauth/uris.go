package oauth

// make them variable because tests may swap the URI
// for a local OAuth server emulator
var (
	// OAuthAuthorizationURI is used to replace URLs in subAPIs in case they are not correct
	OAuthAuthorizationURI = "https://oauth.oit.duke.edu/oauth/authorize.php"
	// OAuthValidationURI is used to validate OAuth tokens
	OAuthValidationURI = "https://oauth.oit.duke.edu/oauth/resource.php"
	// OAuthRegisterAppURI is used to register apps in the OAuth system
	OAuthRegisterAppURI = "https://oauth.oit.duke.edu/oauth-registration-ws/register"
	// OAuthUpdateAppURI is used to update apps in the OAuth system
	OAuthUpdateAppURI = "https://oauth.oit.duke.edu/oauth-registration-ws/update"
	// OAuthRevokeAppURI is used to revoke apps in the OAuth system
	OAuthRevokeAppURI = "https://oauth.oit.duke.edu/oauth-registration-ws/revoke"
	// OAuthRegisterScopeURI is used to register new scopes in the OAuth system
	OAuthRegisterScopeURI = "https://oauth.oit.duke.edu/oauth-registration-ws/addScope"
)
